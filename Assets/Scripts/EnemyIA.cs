using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyIA : MonoBehaviour
{
    
    [SerializeField] private float chaseRange = 10f;
    [SerializeField] private float turningSpeed = 5f;

    private NavMeshAgent _navMeshAgent;
    private float _distanceToTarget = Mathf.Infinity;
    private bool _isProvoked = false;
    private EnemyHealth health;
    private Transform target;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, chaseRange);
    }

    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        health = GetComponent<EnemyHealth>();
        target = FindObjectOfType<PlayerHealth>().transform;
    }
    
    void Update()
    {
        if (health.IsDead())
        {
            enabled = false;
            _navMeshAgent.enabled = false;
        }
        _distanceToTarget = Vector3.Distance(target.position, transform.position);

        if (_isProvoked)
        {
            EngageTarget();
        }
        else if (_distanceToTarget < chaseRange)
        {
            _isProvoked = true;
        }
    }

    private void EngageTarget()
    {
        if (_distanceToTarget >= _navMeshAgent.stoppingDistance)
        {
            GetComponent<Animator>().SetTrigger("Move");
            GetComponent<Animator>().SetBool("Attack", false);

            _navMeshAgent.SetDestination(target.position);
        }
        else
        {
            GetComponent<Animator>().SetBool("Attack", true);
            FaceTarget();
        }
    }

    private void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turningSpeed);
    }

    // Event received within the game object
    public void OnDamageTaken()
    {
        _isProvoked = true;
    }
}
