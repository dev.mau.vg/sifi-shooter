public enum AmmoType {
    Nine_mm,
    ThirtyNine_mm,
    Twenty_Gauge
}